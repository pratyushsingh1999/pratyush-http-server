const fs = require('fs');
const url = require('url');
const http = require('http');
const PORT = 8080;

const routes = (request, response) => {
    const reqUrl = url.parse(request.url , true).pathname;
    const method = request.method;
    let reqUrlArray = reqUrl.split('/');
    reqUrlArray.shift();
    console.log(reqUrl , method , reqUrlArray);
    switch (method) {
        case "GET": {
            switch (reqUrlArray[0]) {
                case "html": {
                    fs.readFile('./public/app.html' , (err ,data) => {
                        if (err) {
                            console.error(err);
                            response.writeHeader(500,{"content-type": "text/html"})
                            response.write("Internal server error");
                            return response.end()
                        }
                        response.writeHeader(200,{'content-type': 'text/html'});
                        response.write(data);
                        return response.end();
                    });
                    break;
                }
                case "json": {
                    fs.readFile('./public/json.json' , (err ,data) => {
                        if (err) {
                            console.error(err);
                            response.writeHeader(500,{"content-type": "text/html"})
                            response.write("Internal server error");
                            return response.end()
                        }
                        response.writeHeader(200,{'content-type': 'application/json'});
                        response.write(data);
                        return response.end();
                    })
                    break;
                }
                case "uuid": {
                    fs.readFile('./public/uuid.json' , (err ,data) => {
                        if (err) {
                            console.error(err);
                            response.writeHeader(500,{"content-type": "text/html"})
                            response.write("Internal server error");
                            return response.end()
                        }
                        response.writeHeader(200,{'content-type': 'application/json'});
                        response.write(data);
                        return response.end();
                    })
                    break;
                }
                case "status": {
                    const statusCode = parseInt(reqUrlArray[1]);
                    if (statusCode == undefined) {
                            response.writeHeader(422,{"content-type": "text/html"})
                            response.write("Status is undefined");
                            return response.end()
                    }
                    // checking wheather the following url has string literals or not
                    if (!(/^\d+$/.test(statusCode))) {
                        response.writeHeader(422,{"content-type": "text/html"})
                        response.write("Status is not a number");
                        return response.end()
                    }
                    // ending response if passed status code is greater than 999
                    if (statusCode<100 || statusCode>1000) {
                        response.writeHeader(422,{"content-type": "text/html"})
                        response.write("Status type greater than 999 and less than 100 does not exist");
                        return response.end()
                    }
                    if (statusCode>99 && statusCode<200) {
                        // response.writeHeader(statusCode,{'content-type': 'text/html'});
                        // response.writeContinue();
                        // response.writeHeader(statusCode,{'content-type': 'text/html'});
                        // response.write('<h1>'+statusCode+'</h1>');
                        // return response.end();
                        // still in development
                    }
                    response.writeHeader(statusCode,{'content-type': 'text/html'});
                    response.write('<h1>'+statusCode+'</h1>');
                    return response.end();
                    break;
                }
                case "delay": {
                    if (reqUrlArray[1] == undefined ) {
                        console.error(err);
                        response.writeHeader(422,{"content-type": "text/html"})
                        response.write("Delay time is undefined");
                        return response.end()
                    }
                    // checking wheather the following url has string literals or not
                    if (!(/^\d+$/.test(reqUrlArray[1]))) {
                        response.writeHeader(422,{"content-type": "text/html"})
                        response.write("Delay is not a number");
                        return response.end()
                    }
                    response.writeHeader(200,{'content-type': 'text/html'});
                    setTimeout(() => {
                        response.write('<h1>'+'Got response after '+reqUrlArray[1]+' Seconds'+'</h1>');
                        return response.end();
                    },parseInt(reqUrlArray[1])*1000);
                    break;
                }
                default: {
                    response.writeHeader(404 , {'content-type': 'text/html'});
                    response.write('<h1>404 Page not found</h1>');
                    response.end();
                }
            }
            break;
        }
        case "POST": {
            response.writeHeader(405);
            response.write("Method not allowed");
            response.end();
            break;
        }
        case "DELETE": {
            response.writeHeader(405);
            response.write("Method not allowed");
            response.end();
            break;
        }
        default: {
            response.writeHeader(405);
            response.write("Method not allowed");
            response.end();
        }
    }
};

const server = http.createServer(routes);
server.listen(PORT);
